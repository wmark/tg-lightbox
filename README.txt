Lightbox
========

This is a TurboGears_ widget wrapper for the Lightbox2_ JavaScript library
by Lokesh Dhakar.

You can view the widgets in the TurboGears Toolbox_.

For further information, visit the `Lightbox widget homepage`_.

.. _turbogears: http://www.turbogears.org/
.. _lightbox2: http://www.lokeshdhakar.com/projects/lightbox2/
.. _toolbox: http://docs.turbogears.org/1.0/Toolbox
.. _lightbox widget homepage: http://docs.turbogears.org/tgLightbox
