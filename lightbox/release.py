# Release information about Lightbox
"""This is a TurboGears_ widget wrapper for the Lightbox2_ JavaScript library
by Lokesh Dhakar.

You can view the widgets in the TurboGears Toolbox_.

For further information, visit the `Lightbox widget homepage`_.

.. _turbogears: http://www.turbogears.org/
.. _lightbox2: http://www.lokeshdhakar.com/projects/lightbox2/
.. _toolbox: http://docs.turbogears.org/1.0/Toolbox
.. _lightbox widget homepage: http://docs.turbogears.org/tgLightbox

"""

version = "2.1.1"

description = "Lightbox photo display widget"
long_description = __doc__
author = "Lokesh Dhakar, Kevin Dangoor, Christopher Arndt"
email = "dangoor+lightbox@gmail.com"
copyright = "Copyright 2006 - 2008"

# if it's open source, you might want to specify these
url = "http://docs.turbogears.org/tgLightbox"
download_url = "http://pypi.python.org/pypi/Lightbox"
license = "MIT"
